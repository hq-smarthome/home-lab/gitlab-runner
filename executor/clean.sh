#!/usr/bin/env bash

set -ueo pipefail

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

if [ -f "$EXECUTOR_JOB_FILE" ]; then
  dispatchId=$(cat "$EXECUTOR_JOB_FILE")

  echo "Stopping executor..."
  nomad job stop -namespace cicd-gitlab "$dispatchId"

  echo "Removed dispatch id file"
  rm "$EXECUTOR_JOB_FILE"
fi

if [ -f "$EXECUTOR_ALLOC_FILE" ]; then
  allocationId=$(cat "$EXECUTOR_ALLOC_FILE")

  # echo "Finishing allocation $allocationId"
  # nomad alloc (clear allocation)

  echo "Removed allocation file"
  rm "$EXECUTOR_ALLOC_FILE"
fi
