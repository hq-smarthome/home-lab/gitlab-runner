#!/usr/bin/env bash

set -ueo pipefail

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

if [ ! -f "$EXECUTOR_ALLOC_FILE" ]; then
  echo "$EXECUTOR_ALLOC_FILE does not exist. Exiting..."
  exit $BUILD_FAILURE_EXIT_CODE
fi

allocationId=$(cat "$EXECUTOR_ALLOC_FILE")

nomad alloc exec -task="executor" -namespace=cicd-gitlab -t=false "$allocationId" /bin/bash < "${1}"
if [ $? -ne 0 ]; then
  echo "Non-zero status from build execution. Exiting..."
  exit $BUILD_FAILURE_EXIT_CODE
fi
