#!/usr/bin/env bash

set -ueo pipefail

/usr/local/bin/gitlab-runner register \
  --non-interactive \
  --url "${CI_SERVER_URL}" \
  --registration-token "${REGISTRATION_TOKEN}" \
  --config "${CONFIG_FILE}" \
  --executor "custom" \
  --shell "bash" \
  --name "${RUNNER_NAME}" \
  --tag-list "${NOMAD_DC},${NOMAD_REGION},${RUNNER_PLATFORM},nomad,terraform,consul,vault,levant,jq" \
  --pre-clone-script "/workspace/scripts/preClone.sh" \
  --pre-build-script "/workspace/scripts/preBuild.sh" \
  --post-build-script "/workspace/scripts/postBuild.sh" \
  --custom-config-exec "/home/gitlab-runner/executor/config.sh" \
  --custom-prepare-exec "/home/gitlab-runner/executor/prepare.sh" \
  --custom-run-exec "/home/gitlab-runner/executor/run.sh" \
  --custom-cleanup-exec "/home/gitlab-runner/executor/clean.sh"

if [ $? -gt 0 ]; then
  echo "Failed to register with gitlab"
  exit 1
fi
