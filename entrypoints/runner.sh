#!/usr/bin/env bash

set -ueo pipefail

RED='\033[0;31m'
NC='\033[0m'

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/setEnvironment.sh # Get variables from base.

vaultAuditMessage() {
  echo -e "${RED} ----- ${NC}"
  echo -e "${RED} Failed to log in using Vault AppRole ${NC}"
  echo -e "${RED} ----- ${NC}"
  echo -e "${RED} A Vault audit should be performed to check if token was previously intercepted. ${NC}"
  echo -e "${RED} If it was unwrapped then it is a sign that it was intercepted. ${NC}"
  echo -e "${RED} ----- ${NC}"

  # Notify a system somewhere automatically here
}

export VAULT_TOKEN=$(vault write -field="token" auth/cicd/login role_id=${ROLE_ID} secret_id=${SECRET_ID})
if [ $? -ne 0 ]; then
  echo "Failed to log into AppRole. Exiting..."
  vaultAuditMessage
  exit 1
fi

/usr/local/bin/gitlab-runner run \
  --config "${CONFIG_FILE}" \
  --working-directory="/home/gitlab-runner/workspace" \
  --user "gitlab-runner" \
  --listen-address ":${NOMAD_PORT_metrics}"
