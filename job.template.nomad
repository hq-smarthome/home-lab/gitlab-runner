job "cicd-gitlab-runner" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]
  namespace = "cicd-gitlab"

  group "linux-amd64" {
    count = 2

    spread {
      attribute = "${node.node.unique.name}"
      weight = 50
    }

    constraint {
      attribute = "${attr.cpu.arch}"
      value = "amd64"
    }

    network {
      port "metrics" { to = 9252 }
    }

    task "register" {
      driver = "docker"

      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      vault {
        policies = ["api-gitlab"]
      }

      config {
        image = "[[ .jobDockerImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        args = ["bash", "-c", "/entrypoints/register.sh"]
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        CONFIG_FILE = "${NOMAD_ALLOC_DIR}/gitlab-runner-config.toml"
        CI_SERVER_URL = "https://gitlab.com/"
        RUNNER_NAME = "${NOMAD_ALLOC_ID}"
        RUNNER_PLATFORM = "linux-amd64"
      }

      template {
        data = <<EOH
          {{ with secret "api/gitlab/runner" }}
          REGISTRATION_TOKEN={{ index .Data.data "registrationToken" }}
          {{ end }}
        EOH

        destination = "secrets/registrationToken.env"
        change_mode = "noop"
        env = true
      }
    }

    task "runner" {
      driver = "docker"

      kill_timeout = "60s"

      vault {
        policies = ["job-cicd-gitlab", "cicd-gitlab-token-generation"]
      }

      config {
        image = "[[ .jobDockerImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        ports = ["metrics"]

        args = ["bash", "-c", "/entrypoints/runner.sh"]
      }

      service {
        name = "gitlab-runner"
        port = "metrics"

        check {
          name = "Metrics Server"
          port = "metrics"
          address_mode = "driver"
          type = "http"
          method = "GET"
          path = "/metrics"
          protocol = "http"
          interval = "30s"
          timeout = "10s"
          task = "runner"
        }
      }

      template {
        data = <<EOH
          {{ with secret "auth/cicd/role/cicd-gitlab-runner/secret-id" "secret_id_num_uses=1" "secret_id_ttl=5m" }}
          SECRET_ID={{ .Data.secret_id }}
          {{ end }}
        EOH
        destination = "secrets/runner.env"
        change_mode = "noop"
        env = true
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        CONFIG_FILE = "${NOMAD_ALLOC_DIR}/gitlab-runner-config.toml"
        CI_SERVER_URL = "https://gitlab.com/"
        RUNNER_NAME = "${NOMAD_ALLOC_ID}"
        RUNNER_PLATFORM = "linux-amd64"

        NOMAD_ADDR = "https://nomad.hq.carboncollins.se"
        NOMAD_NAMESPACE = "cicd-gitlab"
        VAULT_ADDR = "https://vault.hq.carboncollins.se"
        CONSUL_HTTP_ADDR = "https://consul.hq.carboncollins.se"
      }
    }

    task "unregister" {
      driver = "docker"

      lifecycle {
        hook = "poststop"
      }

      config {
        image = "[[ .jobDockerImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        args = ["bash", "-c", "/entrypoints/unregister.sh"]
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        CONFIG_FILE = "${NOMAD_ALLOC_DIR}/gitlab-runner-config.toml"
        CI_SERVER_URL = "https://gitlab.com/"
        RUNNER_NAME = "${NOMAD_ALLOC_ID}"
        RUNNER_PLATFORM = "linux-amd64"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert       = true
    auto_promote      = true
    stagger = "30s"
    canary = 1
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
