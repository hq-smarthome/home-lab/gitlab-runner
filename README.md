# CICD GitLab Runner

> :warning: **This project has been moved**
>
> Any further updates can be found here https://gitlab.com/carboncollins-cloud/cicd/gitlab-runner-container
> This repository will be archived in favour of all further development at the new location.


[[_TOC_]]

## Description

A GitLab runner with a custom executor configured for building and deploying jobs within the hQ Home Lab.

## Basic Usage

These runners with automatically be used if the hQ [CICD Pipeline Template](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/templates) are configured in the `.gitlab-ci.yml` file like so:

```
include:
  - project: '$CICD_TEMPLATE_PROJECT'
    ref: '$CICD_TEMPLATE_REF'
    file: '$CICD_TEMPLATE_FILE'
```
The `CICD_*` variables here are provided by the hQ Group itself.

## Custom Executor

No builds are run on the runner itself and instead all work is passed onto an executor which has all the required build dependencies installed.

The details for what is installed on the executor can be found in the [CICD GitLab Executor Docker](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-docker) repo.

When the runner wants to execute a pipeline job it will dispatch one of the pre-defined paramatised batch jobs (one for each supported architecture) and execute any scripts on that dispatched job. Once all steps are completed the dispatched job will be terminated.

Available executors:
- [Linux AMD64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-amd64)
- ~~[Linux ARM](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-arm)~~ (Planned in the future)
- ~~[Linux ARM64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-arm64)~~ (Planned in the future)
- ~~[Darwin AMD64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-darwin-amd64)~~ (Planned in the future)

## Just-in-Time Credentials

When a job is dispatched to a custom executor a new short lived Vault token is generated for that specific executor instance. This vault token uses the AppRole Auth method where the executor holds the Role ID and the runner gets a wrapped Secret ID to pass to the executor.

This short lived token can then be used to generate short-lived tokens for Nomad & Consul allowing deployments to be authenticated. 

Further reading on how this works can be found in the Vault documentation:
- [Hashicorp Vault - AppRole Pull Authentication](https://learn.hashicorp.com/tutorials/vault/approle)
- [Hashicorp Blog - Authenticating Applications with HashiCorp Vault AppRole](https://www.hashicorp.com/blog/authenticating-applications-with-vault-approle)
- [Hashicorp Nomad - Generate Nomad Tokens with HashiCorp Vault](https://learn.hashicorp.com/tutorials/nomad/vault-nomad-secrets)

## Future Enhancements

The runner and custom executor here is still a work in progress and thus may change. From this I have some enhancements that I want to make:

- `Protected/Non-Protected runners` - I would like to deploy two seperate runner types where protected runners have the permissions to generate tokens that allow deployments and non-protected runners are used for PR builds which wont have permissions to make deployments (at least to the `production` environment) 

- `Job dependant executor tokens` - For this I want the runner to be able to generate tokens for the executor which have limited permissions for that job specifically. 

- `Job architecture selection` - Allow the job to specify which architecture it needs the executor to run on and allow the runner to dispatch the appropriate executor for the job.

## Other Info


This repo has been moved to: https://gitlab.com/carboncollins-cloud/cicd/gitlab-runner-container. This existing repo will remain here archived as there are existing links to this repo.
